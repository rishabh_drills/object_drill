function keys(object = {})
{
    if(!object){
        return [];
    }
    let arr = [];
    for(let key in object)
    {
        arr.push(key);
    }
    return arr;
}

module.exports = keys;