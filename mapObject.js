function mapObject(obj, cb)
{
    if(!obj)
    {
        return {};
    }
    let newObject = {};
    for(let key in obj)
    {
        newObject[key] = cb(obj[key], key);
    }
    return newObject;
}

module.exports = mapObject;