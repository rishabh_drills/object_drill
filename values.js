function values(obj)
{
    if(!obj)
    {
        return [];
    }
    let arr = [];
    let val;
    for(let key in obj)
    {
        val = obj[key];
        if(typeof val === 'function')
        {
            continue;
        }
        arr.push(val);
    }
    return arr;
}

module.exports = values;