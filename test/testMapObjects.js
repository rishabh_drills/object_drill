const testObject = require('../object');
const mapObjects = require('../mapObjects');

const cb = (value, key) => {
    return value + 2;
}

console.log(mapObjects(testObject, cb));

//return value * 2 = { name: NaN, age: 72, location: NaN }
//return value + 2 = { name: 'Bruce Wayne2', age: 38, location: 'Gotham2' }