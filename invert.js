function invert(obj)
{
    if(!obj)
    {
        return {};
    }
    let objInvert = {};
    for(let key in obj)
    {
        let value = obj[key];
        if(typeof(value) !== 'string' || typeof(value) === 'function')
        {
            value = value.toString();
        }
        objInvert[value] = key; 
    }
    return objInvert;
}

module.exports = invert;